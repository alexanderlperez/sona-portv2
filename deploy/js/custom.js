var DEBUG = false;

$(document).ready(function() {
    
    /***************** Smooth Scrolling ******************/
    
    function niceScrollInit(){
        $("html").niceScroll({
            background: '#000',
            scrollspeed: 60,
            mousescrollstep: 35,
            cursorwidth: 15,
            cursorborder: 0,
            cursorcolor: '#FFF',
            cursorborderradius: 6,
            autohidemode: false
        });
        
        $('body, body header').css('padding-right','15px');
    }
    var $smoothActive = $('body').attr('data-smooth-scrolling');
    if( $smoothActive == 1 && $(window).width() > 1024){ niceScrollInit(); }
    /*ONE PAGE NAV*/
    $('.nav.js nav').onePageNav({
      filter : ':not(.external)',
      scrollThreshold : 0.25
    });
    $('.mob_nav.js ul').onePageNav({
        filter : ':not(.external)',
        scrollThreshold : 0.25
    });
    $('.trigger').toggle(function(){
        $(this).next().slideDown('normal');
    },function(){
        $(this).next().slideUp('normal');
    });
    /* NAV */
   var aNav = $('nav a');
   
    $('nav a').each(function() {
        $(this).wrapInner('<span class="menu_name"></span>');
        $(this).append('<span class="hover"><span class="arr"></span></span>');
    });
    
    /* BORDERS */
    if($(window).width() > 1024) var w = $(window).width()-15; 
    else var w = $(window).width();
    $('.top_box_left, .top_box_right').css('border-left-width', w);
    $('.bot_box_left, .bot_box_right').css('border-right-width', w);
    
    /* CUSTOM */
    $(".flexslider").fitVids();
    $('.lines').each(function(){
       $(this).wrapInner('<span class="plug"></span>');
    }); 
    
    $('.back2top').click(function(){
       $('html, body').animate({scrollTop : 0},'slow');
        return false;
    });
    
   
    
    $('.team_photo').hover(function(){
        $(this).find('.team_post').fadeIn('normal');
    },function(){
        $(this).find('.team_post').fadeOut('normal');
    });
    
    $('.speed_box:nth-child(2n+1)').addClass('ipad');
    
    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
    $('form').submit(function() {
        var a = $(this).find('input[name="name"]').val();
        var b = $(this).find('input[name="email"]').val();
        var c = $(this).find('textarea[name="msg"]').val();
        if (a == "") {
            $('.error_checker').fadeIn("slow").text("Type your name please!");
            return false;
        }
        if (validateEmail(b)) {
        } else {
            $('.error_checker').fadeIn("slow").text("Type your email correctly please!");
            return false;
        }
        if (c == "") {
            $('.error_checker').fadeIn("slow").text("Type your message please!");
            return false;
        }
        else {
            $.ajax({
                type: $(this).attr('method'),
                url: $(this).attr('action'),
                data: $(this).serialize(),
                success: function (data) {
					$.post("sendmail.php", $("#form-send-message").serialize(), function() {
						$("#form-send-message").each(function() {
							this.reset();
							$("#message-sent-ok").delay(300).show(2000).delay(5000).fadeOut(1000);
						});
					});
                }
        });
                                            
        return false;
        }
    });
    
    $('.slide_link').click(function(){
        var target = $(this).attr('href');
        $('html').scrollTo( target, 800 );
        
    });

   
});
/*      SLIDER  */
$(window).load(function() {
    $('body').animate({opacity : 1}, 1000);
    $('.flexslider').flexslider({
        animation : "slide",
        controlNav: false,
        video: true
    });
    $('.latest_tweet').tweet({
            modpath: "twitter/",                     
            join_text: "auto",
            username: ["GoGetThemes"],
            count: 2,
            auto_join_text_url: "",
            loading_text: "loading tweets...",
            ul_class: "sidebar_tweet",
            template: "{text}{time}"
    });
    $('a.soc_font').tooltip();
    $('.fsoc').tooltip();
});

$(window).resize(function() {
    var w = $(window).width();
    if (w < 768)
        var ipadw = w;
    if (w > 768)
        var ipadw = w-15;

    $('.top_box_left, .top_box_right').css('border-left-width', w);
    $('.bot_box_left, .bot_box_right').css('border-right-width', w);
});

// deal with validating and revealing the thank you message
$('#message-sent-ok').hide();
if(DEBUG) {
	$('#contact-submit').click(function (e) {
		e.preventDefault();
		$("#form-send-message").each(function() {
			this.reset();
			$("#message-sent-ok").delay(300).show(2000).delay(5000).hide(1000);
		});
	});
}
/*
$("#form-send-message").validate({ 
	errorPlacement: function(error, element) {},
		submitHandler: function(form) {
			console.log('form button clicked');
			$.post("sendmail.php", $("#form-send-message").serialize(), function() {
				$("#form-send-message").each(function() {
					this.reset();
					$("#message-sent-ok").delay(300).show(2000).delay(5000).hide(1000);
				});
			});
		}   
});
*/

// my code is on lines 1 - 118 and 168 - 195, the rest is initialization and
// event handling code from the Parasponsive theme
// See the galleryControl code being used on lines 170 - 181
var galleryControl = (function ($) {

  function createEntry (category, path) {
      return $('<div class="isotope-item ' + category + '">' +
              '<div class="iso_inner">' + 
              '<a href="image/custom/' + path + '" data-fresco-group="mixed" class="fresco">' +
              '<img src="image/custom/previews/'+ path + '" alt="" />' +
              '<div class="over_box">' +
              '<div class="over_box_pad">' +
              '<div class="over_box_inner">' +
              '</div>' +
              '</div>' +
              '</div>' +
              '</a>' +
              '</div>' +
              '</div>'); 
  }

  var categoryArrayObj = {};
  var numberOfCategories = 0;

  function appendIsotopeItem(item, category, elem) {
    var $entry = createEntry(category, item);
    $(elem).isotope('insert', $entry);
  }

  function appendAndRemoveIsotopeItem (list, item, category, elem) {
    appendIsotopeItem(item, category, elem);
    list.splice(0,1);

  }

  // grab from a single category and remove items 
  // from the top of the category item lists/queues
  $.fn.loadGalleryItems = function (list, category, number) {
    return this.each(function (v,i,a) {
      for (var i = 0; i < number; i++) {
        item = list[category][0];
        if (item) {
          appendAndRemoveIsotopeItem(list[category], item, category, this);
        }
      }
    });
  };

  // initialize values and set up all the gallery categories
  // with appended items
  $.fn.setupGallery = function (options) {
    var initialItems = options.initialItemsPerCat;
    categoryArrayObj = $.extend({}, categoryArrayObj, options.categoryArrayObj);
    
    this.each(function (v,i,a) {
      for (var cat in categoryArrayObj) {
        if (categoryArrayObj.hasOwnProperty(cat)) {
          for (var i = 0; i < initialItems; i++) {
            //grab from the beginning of the queue to remove later
            item = categoryArrayObj[cat][0];
            if (item) {
              appendAndRemoveIsotopeItem(categoryArrayObj[cat], item, cat, this);
            }
          }
        }
      }
    });

  };
})(jQuery);


function createPortItemsArrays (arrList, categories) {
  var portItems = {};
  categories.forEach(function (v,i,a) {
    portItems[v] = new Array();
  });
  arrList.forEach(function (v,i,a) {
    portItems[v.category].push(v.path);
  });
  return portItems;
}

var list = [
  { path: 'portfolio_img10.jpg', category: 'photo' },
  { path: 'portfolio_img11.jpg', category: 'photo' },
  { path: 'portfolio_img12.jpg', category: 'photo' },
  { path: 'portfolio_img13.jpg', category: 'photo' },
  { path: 'portfolio_img14.jpg', category: 'photo' },
  { path: 'portfolio_img15.jpg', category: 'photo' },
  { path: 'portfolio_img16.jpg', category: 'photo' },
  { path: 'portfolio_img17.jpg', category: 'photo' },
  { path: 'portfolio_img18.jpg', category: 'photo' },
  { path: 'portfolio_img19.jpg', category: 'photo' },
  { path: 'portfolio_img1.jpg', category: 'photo' },
  { path: 'portfolio_img20.jpg', category: 'photo' },
  { path: 'portfolio_img21.jpg', category: 'photo' },
  { path: 'portfolio_img22.jpg', category: 'photo' },
  { path: 'portfolio_img23.jpg', category: 'photo' },
  { path: 'portfolio_img24.jpg', category: 'photo' },
  { path: 'portfolio_img25.jpg', category: 'photo' },
  { path: 'portfolio_img26.jpg', category: 'photo' },
  { path: 'portfolio_img2.jpg', category: 'photo' },
  { path: 'portfolio_img3.jpg', category: 'photo' },
  { path: 'portfolio_img4.jpg', category: 'photo' },
  { path: 'portfolio_img5.jpg', category: 'photo' },
  { path: 'portfolio_img6.jpg', category: 'photo' },
  { path: 'portfolio_img7.jpg', category: 'photo' },
  { path: 'portfolio_img8.jpg', category: 'photo' },
  { path: 'portfolio_img9.jpg', category: 'photo' },
  { path: 'portfolio_web1.jpg', category: 'web' },
  { path: 'portfolio_web2.jpg', category: 'web' },
  { path: 'portfolio_web3.jpg', category: 'web' },
  { path: 'portfolio_graphic4.jpg', category: 'graphics' },
  { path: 'portfolio_graphic5.jpg', category: 'graphics' },
  { path: 'portfolio_graphic3.jpg', category: 'graphics' },
  { path: 'portfolio_graphic1.png', category: 'graphics' },
  { path: 'portfolio_graphic2.png', category: 'graphics' },
  { path: 'portfolio_graphic6.png', category: 'graphics' },
  { path: 'portfolio_graphic7.png', category: 'graphics' },
];

$(function() {
    var $container = $('#container');
    var winW = $(window).width();
    if (winW < 768)
        var colW = $container.width() / 2
    if (winW > 768)
        var colW = $container.width() / 3

    $container.isotope({
        itemSelector : '.isotope-item',
        masonry : {
            columnWidth : colW
        }
    });

    var $optionSets = $('#options .option-set'), $optionLinks = $optionSets.find('a');

    $optionLinks.click(function() {
        var $this = $(this);
        // don't proceed if already selected
        if ($this.hasClass('selected')) {
            return false;
        }
        var $optionSet = $this.parents('.option-set');
        $optionSet.find('.selected').removeClass('selected');
        $this.addClass('selected');

        // make option object dynamically, i.e. { filter: '.my-filter-class' }
        var options = {}, key = $optionSet.attr('data-option-key'), value = $this.attr('data-option-value');
        // parse 'false' as false boolean
        value = value === 'false' ? false : value;
        options[key] = value;
        if (key === 'layoutMode' && typeof changeLayoutMode === 'function') {
            // changes in layout modes need extra logic
            changeLayoutMode($this, options)
        } else {
            // otherwise, apply new options
            $container.isotope(options);
        }

        return false;
    });
    
}); 

$(document).ready(function () {
  // galleryControls
  var categoryArrays = createPortItemsArrays(list, ['web', 'graphics', 'photo']); 
  $('#container').setupGallery({
    categoryArrayObj: categoryArrays,
    initialItemsPerCat: 6
  });
  
  $('.load_more').click(function (e) {
    e.preventDefault();
    // get the current category
    category = $('#filters a[class="selected"]').attr('data-option-value').slice(1);
    $('#container').loadGalleryItems(categoryArrays, category, 3);
  });

  // force Isotope to organize the newly generated items after everything has
  // been loaded
  $("#container").isotope('reLayout');

  // sure the photo category is loaded
  // once everything has loaded
  $(window).load(function () {
    $('a[data-option-value=".photo"]').click();
    //var iso_w = $('.isotope-item').width()-30;
    var iso_h = $('.isotope-item').height()-60;
    $('.over_box_inner').height(iso_h);
    });
});

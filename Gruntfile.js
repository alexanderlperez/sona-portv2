// don't forget to create the .ftppass file with the login credentials as a JSON file (check onlie for format)
module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    'ftp-deploy': {
      build: {
        auth: {
          host: 'ftp.sonacitypro.com',
          port: 21,
          authKey: 'key1'
        },
        src: 'deploy/',
        dest: '/www',
        exclusions: ['sass/', 'includes/', '*.jade', '.*', '*.rb', '*.vim', 'Gruntfile*', '.ftppass', '*.swp']
      }
    }
  });

  grunt.loadNpmTasks('grunt-ftp-deploy');

  grunt.registerTask('default', 'ftp-deploy');

};
